// Handle for the Date conversion request
function handleDate (req, res) {
    // Check for provided dateString & convert to Date object
    const date = req.params.date || Date.now();
    const currentDate = isNaN(date) ? new Date(date) : new Date(Number(date));

    try {
        // Check if the date is valid
        if (isNaN(currentDate.getTime())) {
            res.status(400).json({ error: 'Invalid Date' });
            return;
        }

        res.json({
            unix: currentDate.getTime(),
            utc: currentDate.toUTCString()
        });
    } catch (err) {
        res.status(401).json({ error: 'Invalid Date' });
    }
}

module.exports = {
    handleDate
}
