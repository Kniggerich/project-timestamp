const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../index');
const expect = chai.expect;

chai.use(chaiHttp);

describe('Date Handler', function() {
    describe('GET /api/:date?', function() {
        it('should return a unix timestamp and a UTC string for valid dates', function(done) {
            chai.request(server)
                .get('/api/2023-08-18')
                .end((err, res) => {
                    expect(err).to.be.null;
                    expect(res).to.have.status(200);
                    expect(res.body).to.have.property('unix').that.is.a('number');
                    expect(res.body).to.have.property('utc').that.is.a('string');
                    done();
                });
        });

        it('should return a unix timestamp and a UTC string for valid timestamps', function(done) {
            chai.request(server)
                .get('/api/1451001600000')
                .end((err, res) => {
                    expect(err).to.be.null;
                    expect(res).to.have.status(200);
                    expect(res.body).to.have.property('unix').that.equals(1451001600000);
                    expect(res.body).to.have.property('utc').that.equals("Fri, 25 Dec 2015 00:00:00 GMT");
                    done();
                });
        });
    });
});
