// apiTests.mjs

import test from 'node:test';
import assert from 'assert';
import http from 'http';

// Helper function to make GET requests
function getApi(date) {
    return new Promise((resolve, reject) => {
        http.get(`http://localhost:3000/api/${date}`, (res) => {
            let data = '';

            res.on('data', (chunk) => {
                data += chunk;
            });

            res.on('end', () => {
                resolve(JSON.parse(data));
            });
        }).on('error', (err) => {
            reject(err);
        });
    });
}

test('GET /api/:date with full date', async () => {
    const response = await getApi('2023-08-18');
    assert.strictEqual(typeof response.unix, 'number');
    assert.strictEqual(typeof response.utc, 'string');
});

test('GET /api/:date with month/year date', async () => {
    const response = await getApi('2023-08');
    assert.strictEqual(typeof response.unix, 'number');
    assert.strictEqual(typeof response.utc, 'string');
});

test('GET /api/:date with date in DD-MM-YYYY format', async () => {
    const response = await getApi('18-08-2023');
    // Assuming your API returns an 'Invalid Date' error for non-standard formats
    assert.strictEqual(response.error, "Invalid Date");
});

test('GET /api/:date with just a year', async () => {
    const response = await getApi('2023');
    assert.strictEqual(typeof response.unix, 'number');
    assert.strictEqual(typeof response.utc, 'string');
});

test('GET /api/:date with an empty parameter', async () => {
    const response = await getApi('');
    assert.strictEqual(typeof response.unix, 'number');
    assert.strictEqual(typeof response.utc, 'string');
    // Optionally, you can also check if it's relatively close to the current time:
    const now = Date.now();
    assert.ok(Math.abs(response.unix - now) < 5000, 'Returned time is more than 5 seconds away from current time');
});
